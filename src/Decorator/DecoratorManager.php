<?php
namespace Decorator;

use Contract\DataProviderInterface;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerInterface;

class DecoratorManager implements DataProvider
{
    private $provider;
    private $cache;
    private $logger;

    public function __construct(DataProviderInterface $provider,
                                CacheItemPoolInterface $cache,
                                LoggerInterface $loger)
    {
        $this->provider = $provider;
        $this->cache = $cache;
        $this->logger = $loger;
    }

    /**
     * @param RequestDto $request
     *
     * @return ResponseDto
     */
    public function get(RequestDto $request): ResponseDto
    {
        $rawDecorator = new DecoratorRawData($this->provider);
        $cachedDecorator = new DecoratorCachedData($rawDecorator);
        $throwableDecorator = new DecoratorThrowableData($cachedDecorator);

        return $throwableDecorator->get($request);
    }
}