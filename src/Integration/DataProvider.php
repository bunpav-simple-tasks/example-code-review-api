<?php

namespace Integration;

use Psr\Http\Client\ClientInterface;
use Contract\DataProviderInterface;

class DataProvider implements DataProviderInterface
{
    private $client;

    /**
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param RequestDto $request
     *
     * @return ResponseDto
     */
    public function get(RequestDto $request): ResponseDto
    {
        // returns a response from external service
    }
}